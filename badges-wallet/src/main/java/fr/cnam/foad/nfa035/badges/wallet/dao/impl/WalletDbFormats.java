package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

public enum WalletDbFormats {

    jsonBadge,
    directAccess,
    simple;
}
